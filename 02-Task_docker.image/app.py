from flask import Flask
import datetime

app = Flask(__name__)

@app.route('/date')
def get_date():
    current_time = datetime.datetime.now().time()
    seconds = current_time.second
    time_str = current_time.strftime("%H:%M:%S")
    time_type = "ODD" if seconds % 2 != 0 else "EVEN"
    return f"{time_str} {time_type}"

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80)

